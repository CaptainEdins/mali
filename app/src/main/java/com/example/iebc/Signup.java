package com.example.iebc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Signup extends AppCompatActivity {

    EditText username,password,first,last,id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initValues();
    }


    public void initValues() {
        username = findViewById(R.id.user);
        password = findViewById(R.id.password);
        last = findViewById(R.id.last);
        id = findViewById(R.id.id);
        first = findViewById(R.id.first);
    }

    public void back(View view) {
        finish();
    }

    public void Signup(View view) {
        checkIfEmpty();
    }

    public void checkIfEmpty() {
        if (username.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"username can't be empty",Toast.LENGTH_LONG).show();
        }else if (password.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"password can't be empty",Toast.LENGTH_LONG).show();
        }else if (id.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"id can't be empty",Toast.LENGTH_LONG).show();
        }else if (last.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"last name can't be empty",Toast.LENGTH_LONG).show();
        }else if (first.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"first name can't be empty",Toast.LENGTH_LONG).show();
        }else {
            checkDetails();
        }
    }

    public void checkDetails() {
        SharedPreferences share = getSharedPreferences("details", Context.MODE_PRIVATE);

        SharedPreferences.Editor myEdit = share.edit();

        myEdit.putString("user", username.getText().toString());
        myEdit.putString("pass",password.getText().toString());
        myEdit.putString("id",id.getText().toString());
        myEdit.putString("first",first.getText().toString());
        myEdit.putString("last",last.getText().toString());

        myEdit.commit();
        myEdit.apply();

        Toast.makeText(this,"registered",Toast.LENGTH_LONG).show();


        finish();

    }
}