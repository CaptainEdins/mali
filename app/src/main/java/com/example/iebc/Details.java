package com.example.iebc;

import androidx.appcompat.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }

    public void logout(View view) {
        SharedPreferences settings = getSharedPreferences("details", Context.MODE_PRIVATE);
        settings.edit().clear().commit();

        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
}