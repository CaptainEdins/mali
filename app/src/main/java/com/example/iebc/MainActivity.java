package com.example.iebc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initValues();
    }


    public void initValues() {
        username = findViewById(R.id.user);
        password = findViewById(R.id.password);
    }

    public void Signup(View view) {
        startActivity(new Intent(this,Signup.class));
    }
    public void checkIfEmpty() {
        if (username.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"username can't be empty",Toast.LENGTH_LONG).show();
        }else if (password.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"password can't be empty",Toast.LENGTH_LONG).show();
        }else {
            checkDetails();
        }
    }

    public void checkDetails() {
        SharedPreferences share = getSharedPreferences("details", Context.MODE_PRIVATE);
        String getUser = share.getString("user","");
        String getPassword = share.getString("pass","");

        Log.d("muli",getPassword);
        Log.d("muli",getUser);
        Log.d("muli",password.getText().toString().trim());
        Log.d("muli",username.getText().toString().trim());

        String user = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if (pass.equals(getPassword) && user.equals(getUser)){
            //open details
            startActivity(new Intent(this,Details.class));
            finish();
        }else {
            Toast.makeText(this,"username or password incorrect!",Toast.LENGTH_LONG).show();
        }
    }
    public void login(View view) {
        checkIfEmpty();
    }
}